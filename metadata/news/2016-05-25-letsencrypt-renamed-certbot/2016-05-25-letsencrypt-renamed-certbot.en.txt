Title: dev-python/letsencrypt has been renamed to dev-python/certbot
Author: Arnaud Lefebvre <a.lefebvre@outlook.fr>
Content-Type: text/plain
Posted: 2016-05-25
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-python/letsencrypt

Letsencrypt client has been renamed to certbot.
Note that the binary is now called certbot instead of letsencrypt.
Please install dev-python/certbot and uninstall dev-python/letsencrypt:

cave resolve dev-python/certbot -r dev-python/letsencrypt
