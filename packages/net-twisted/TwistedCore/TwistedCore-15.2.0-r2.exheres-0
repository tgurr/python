# Copyright 2008, 2009 Ali Polatel
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'twisted-8.1.0.ebuild' from Gentoo, which is:
#   Copyright 1999-2008 Gentoo Foundation

TWISTED_MODULE_NAMES=(
    'twisted.application'
    'twisted.cred'
    'twisted.internet'
    'twisted.logger'
    'twisted.manhole'
    'twisted.persisted'
    'twisted.positioning'
    'twisted.protocols'
    'twisted.python'
    'twisted.scripts'
    'twisted.test'
    'twisted.trial'
)

TWISTED_DISABLE_TESTS=(
    # multicast stuff
    test_loopback test/test_udp.py
    test_multicast test/test_udp.py
    test_multiListen test/test_udp.py

    # fails if not all Twisted modules are installed
    test_exist python/test/test_dist3.py

    test_genZshFunction python/test/test_shellcomp.py
    test_basicOperation scripts/test/test_tap2deb.py

    # fails if TwistedLore is not installed
    test_loreDeprecation test/test_twisted.py

    # fails if TwistedConch is not installed
    test_isChecker cred/test/test_strcred.py

    test_inspectCertificate test/test_sslverify.py
)

require zsh-completion twisted

SUMMARY="An asynchronous networking framework written in Python"

SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

# TODO: Firebird, Gadfly, PyPgSQL, PyPAM
DEPENDENCIES="
    build+run:
        dev-python/zopeinterface[>=4.0.0][python_abis:*(-)?]
    recommendation:
        dev-python/pyopenssl[>=0.11][python_abis:*(-)?] [[ description = [ SSL support ] ]]
    suggestion:
        dev-python/mysql-python[python_abis:*(-)?] [[ description = [ MySQL support ] note = [ untested ] ]]
        dev-python/psycopg2[python_abis:*(-)?] [[ description = [ PostgreSQL support ] note = [ untested ] ]]
        dev-python/pyserial[python_abis:*(-)?] [[ description = [ Serial port support ] ]]
        dev-python/pysqlite[python_abis:*(-)?] [[ description = [ Sqlite support ] note = [ untested ] ]]
        gnome-bindings/pygtk:2[python_abis:*(-)?] [[ description = [ GUI support via GTK+ ] ]]
        dev-util/glade[python][python_abis:*(-)?] [[ description = [ UI Designer support via Glade ] ]]
    test:
        dev-python/setuptools[python_abis:*(-)?]
"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}/${PNV}-use-localhost.patch" )

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( CREDITS NEWS README )

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # needs TwistedWeb, which might not be available
    edo rm -f twisted/python/test/test_release.py
}

test_one_multibuild() {
    unset DISPLAY

    esandbox allow_net "unix:${TEMP%/}/*"
    esandbox allow_net "unix:${TEMP%/}/tests/*"
    esandbox allow_net "unix:${TEMP%/}/tests/*/*/*/*/temp"
    esandbox allow_net "unix-abstract:*"
    esandbox allow_net --connect "inet:127.0.0.1@69"
    esandbox allow_net --connect "inet:127.0.0.1@80"

    # some tests fail with LC_ALL=C because they assume an encoding
    # to which Unicode strings can be converted
    LC_ALL=en_GB.UTF-8 twisted_test_one_multibuild

    esandbox disallow_net "unix:${TEMP%/}/*"
    esandbox disallow_net "unix:${TEMP%/}/tests/*"
    esandbox disallow_net "unix:${TEMP%/}/tests/*/*/*/*/temp"
    esandbox disallow_net "unix-abstract:*"
    esandbox disallow_net --connect "inet:127.0.0.1@69"
    esandbox disallow_net --connect "inet:127.0.0.1@80"
}

install_one_multibuild() {
    twisted_install_one_multibuild
    dozshcompletion twisted/python/twisted-completion.zsh
}

