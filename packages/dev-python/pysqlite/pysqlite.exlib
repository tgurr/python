# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'pysqlite-2.5.5.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require pypi
require setup-py [ import=distutils blacklist="3" min_versions="2.7" ]

SUMMARY="Python wrapper for the local database Sqlite"
DESCRIPTION="
SQLite is a powerful embedded relational database engine. pysqlite makes it
available to Python developers via the Database API 2.0.
"
#BUGS_TO="philantrop@exherbo.org"

LICENCES="pysqlite"
SLOT="2"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-db/sqlite:3[>=3.1]
"

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # don't install pysqlite2.test
    edo sed -i -e 's/, "pysqlite2.test"//' setup.py
    # workaround to make checks work without installing them
    edo sed -i -e "s/pysqlite2.test/test/" lib/test/__init__.py
    # correct encoding
    edo sed -e "s/\(coding: \)ISO-8859-1/\1utf-8/" \
        -i lib/__init__.py \
        -i lib/dbapi2.py
}

test_one_multibuild() {
    edo pushd lib
    PYTHONPATH="$(ls -d ../build/lib.*)" "${PYTHON}" -c \
        "from test import test;import sys;sys.exit(test())"
    if ! [[ $? == 1 ]]; then
        die "tests failed"
    fi
    edo popd
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    edo rm -r "${IMAGE}"/usr/pysqlite2-doc
}

