# Copyright 2010-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools blacklist=none ]

export_exlib_phases src_test_expensive

SUMMARY="compiler for writing C extensions for the Python language"
DESCRIPTION="
Cython is a language that makes writing C extensions for the Python language as
easy as Python itself. It is based on the well-known Pyrex, but supports more cutting
edge functionality and optimizations.
"
HOMEPAGE="http://${PN,}.org"

BUGS_TO="philantrop@exherbo.org"

UPSTREAM_CHANGELOG="http://trac.${PN,}.org/${PN,}_trac/query?group=component&milestone=${PV}"
UPSTREAM_DOCUMENTATION="http://docs.${PN,}.org/ [[ lang = en ]]"

LICENCES="Apache-2.0"
SLOT="0"
MYOPTIONS="doc examples"

# tests are expensive, even when run in parallel
RESTRICT="test"

DEPENDENCIES="
    test-expensive:
        dev-python/numpy[python_abis:*(-)?]
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( ToDo.txt USAGE.txt )

Cython_src_test_expensive() {
    easy-multibuild_run_phase
}

test_expensive_one_multibuild() {
    emake -j1 test TESTOPTS="-j${EXJOBS:-1}"
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    if option doc ; then
        insinto /usr/share/doc/${PNVR}/html
        doins -r Doc/*
    fi

    if option examples; then
        insinto /usr/share/doc/${PNVR}/examples
        doins -r Demos/*
    fi
}

