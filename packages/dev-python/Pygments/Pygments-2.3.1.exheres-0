# Copyright 2008, 2009, 2010 Ali Polatel
# Copyright 2015-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require bash-completion pypi setup-py [ import=setuptools blacklist="3.2" test=nose has_bin=true ]

SUMMARY="Syntax highlighting package written in Python"
HOMEPAGE="http://pygments.org"
LICENCES="BSD-3"
DESCRIPTION="
Pygments is a generic syntax highlighter for general use in all kinds of
software such as forum systems, wikis or other applications that need to
prettify source code. Highlights are:
* A wide range of common languages and markup formats is supported.
* Special attention is paid to details that increase highlighting quality.
* Support for new languages and formats are added easily; most languages use a
  simple regex-based lexing mechanism.
* A number of output formats is available, among them HTML, RTF, LaTeX and ANSI
  sequences.
* It is usable as a command-line tool and as a library.
* ... and it highlights even Brainf*ck!
"
BUGS_TO="polatel@itu.edu.tr philantrop@exherbo.org"
REMOTE_IDS="pypi:Pygments"

SLOT="0"
PLATFORMS="~amd64 ~arm ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    test:
        dev-python/nose[python_abis:*(-)?]
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( CHANGES )

test_one_multibuild() {
    # Skip broken test_cmdline on Python 2.x
    # https://bitbucket.org/birkenfeld/pygments-main/issues/1492
    if [[ $(python_get_abi) == 2.* ]]; then
        edo rm tests/test_cmdline.py
    fi

    emake -j1 test
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    doman doc/pygmentize.1

    dobashcompletion external/pygments.bashcomp pygments
}

