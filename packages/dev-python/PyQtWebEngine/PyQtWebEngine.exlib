# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'PyQt5.exlib', which is:
#     Copyright 2008-2016 Wulf C. Krueger <philantrop@exherbo.org>
#     Copyright 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>

myexparam sip_version

MY_PNV="${PN}_gpl-${PV%.0}"
require pypi
require python [ blacklist=none multiunpack=true work=${MY_PNV} ]

SUMMARY="PyQt5 is a set of Python bindings for the Qt WebEngine framework"
DESCRIPTION="The framework provides the ability to embed web content in
applications and is based on the Chrome browser. The bindings sit on top of
PyQt5 and are implemented as three separate modules corresponding to the
different libraries that make up the framework."

BASE_URI="http://www.riverbankcomputing.com"
HOMEPAGE="${BASE_URI}/software/pyqt/download5/"
DOWNLOADS="${BASE_URI}/static/Downloads/${PN}/${MY_PNV}.tar.gz"

UPSTREAM_CHANGELOG="${BASE_URI}/static/Downloads/${PN}/ChangeLog"
UPSTREAM_RELEASE_NOTES="${BASE_URI}/news/pyqt-$(ever major)$(ever range 2)"

SLOT="0"
LICENCES="GPL-3"
MYOPTIONS="debug"

DEPENDENCIES="
    build+run:
        dev-python/PyQt5[python_abis:*(-)?][webchannel]
        dev-python/sip[>=$(exparam sip_version)][python_abis:*(-)?]
        x11-libs/qtbase:5
        x11-libs/qtwebengine:5
       !dev-python/PyQt5[<5.12][python_abis:*(-)?][webengine] [[
            description = [ PyQtWebEngine was split out from PyQt5 ]
            resolution = [ uninstall-blocked-after ]
        ]]
"

DEFAULT_SRC_INSTALL_PARAMS=( INSTALL_ROOT="${IMAGE}" )

prepare_one_multibuild() {
    python_prepare_one_multibuild

    # When system python is set to 2.6 python_bytecompile() can't process
    # source files that use python 3 syntax. See Gentoo bug #274499.
    [[ $(python_get_abi) == 2.* ]] && edo rm -rf pyuic/uic/port_v3
    [[ $(python_get_abi) == 3.* ]] && edo rm -rf pyuic/uic/port_v2
}

configure_one_multibuild() {
    local myparams=(
        --destdir $(python_get_sitedir)/PyQt5
        --qmake /usr/$(exhost --target)/lib/qt5/bin/qmake
        --sipdir /usr/share/sip/PyQt5
        --sip-incdir $(python_get_incdir)
        # Get installed to /usr/lib/... and we probably don't need them
        --no-dist-info
        # Disable PyQt API file for QScintilla
        --no-qsci-api
        --verbose
        $(option debug && echo '--debug')
    )

    edo ${PYTHON} configure.py "${myparams[@]}"
}

install_one_multibuild() {
    default

    python_bytecompile

    edo mv \
        "${IMAGE}"/usr/lib/python$(python_get_abi)/site-packages/PyQt5/*.pyi \
        "${IMAGE}"/$(python_get_sitedir)/PyQt5/
    edo find "${IMAGE}"/usr/lib -type d -empty -delete
}

