# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

# Python 3 only because ipython is the only dependent.
require pypi setup-py [ blacklist=2 import=setuptools test=pytest ]

SUMMARY="Interactive Parallel Computing with IPython"
DESCRIPTION="
Use multiple instances of IPython in parallel, interactively.
"
HOMEPAGE="https://ipyparallel.readthedocs.io/"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# Wants ipython[test] for testing. Here is excerpt from it for convenience:
# test = ['nose>=0.10.1', 'requests', 'testpath', 'pygments', 'nbformat', 'ipykernel', 'numpy']
DEPENDENCIES="
    build+run:
        dev-python/decorator[python_abis:*(-)?]
        dev-python/ipykernel[python_abis:*(-)?]
        dev-python/ipython[>=4][parallel][python_abis:*(-)?]
        dev-python/ipython_genutils[python_abis:*(-)?]
        dev-python/jupyter_client[python_abis:*(-)?]
        dev-python/python-dateutil[>=2.1][python_abis:*(-)?]
        dev-python/pyzmq[>=13][python_abis:*(-)?]
        dev-python/tornado[>=4.0][python_abis:*(-)?]
        dev-python/traitlets[>=4.3][python_abis:*(-)?]
    test:
        dev-python/ipykernel[>=4.4][python_abis:*(-)?]
        dev-python/ipython[python_abis:*(-)?]
        dev-python/mock[python_abis:*(-)?]
        dev-python/nbformat[python_abis:*(-)?]
        dev-python/nose[>=0.10.1][python_abis:*(-)?]
        dev-python/numpy[python_abis:*(-)?]
        dev-python/Pygments[python_abis:*(-)?]
        dev-python/pytest-cov[python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
        dev-python/testpath[python_abis:*(-)?]
"

PYTEST_PARAMS=(
    -vs
    # Failing under paludis, last checked: 6.2.3
    -k "not TestSSHEngineLauncher and not TestSSHProxyEngineSetLauncher and not TestSSHControllerLauncher"
)

src_test() {
    local local_port_range="32768-60999"
    local wl_item whitelist=(
        "inet:0.0.0.0@0"
        "--connect LOCAL@${local_port_range}"
    )

    for wl_item in "${whitelist[@]}"; do
        esandbox allow_net "${wl_item}"
    done
    setup-py_src_test
    for wl_item in "${whitelist[@]}"; do
        esandbox disallow_net "${wl_item}"
    done
}

