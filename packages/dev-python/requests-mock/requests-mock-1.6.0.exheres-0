# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools test=pytest ]

SUMMARY="Provides a building block to stub out the HTTP requests portions of your testing code"
DESCRIPTION="
The requests library has the concept of pluggable transport adapters. These
adapters allow you to register your own handlers for different URIs or
protocols.
The requests-mock library at its core is simply a transport adapter that can be
preloaded with responses that are returned if certain URIs are requested. This
is particularly useful in unit tests where you want to return known responses
from HTTP requests without making actual calls.
"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# tests do network stuff (test_request.py)
# missing test dependency: testrepository
RESTRICT="test"

DEPENDENCIES="
    build+run:
        dev-python/fixtures[python_abis:*(-)?]
        dev-python/pbr[python_abis:*(-)?]
        dev-python/requests[>=2.3][python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
    test:
        dev-python/Sphinx[python_abis:*(-)?]
        dev-python/mock[python_abis:*(-)?]
        dev-python/purl[python_abis:*(-)?]
        dev-python/testtools[python_abis:*(-)?]
"

