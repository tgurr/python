# Copyright 2013 Benedikt Morbach <moben@exherbo.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'pylint-1.7.2.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation

require pypi
if ever at_least 2; then
    require setup-py [ blacklist=2 import=setuptools has_bin=true test=pytest ]
else
    require setup-py [ import=setuptools has_bin=true test=pytest ]
fi

export_exlib_phases src_test_expensive

SUMMARY="a python code static checker"
DESCRIPTION="
Pylint is a Python source code analyzer which looks for programming errors, helps enforcing a coding
standard and sniffs for some code smells (as defined in Martin Fowler's Refactoring book).

It's highly configurable and handle pragmas to control it from within your code.
Additionally, it is possible to write plugins to add your own checks.
"
HOMEPAGE="https://www.pylint.org/"

LICENCES="|| ( GPL-2 GPL-3 )"
SLOT="0"

MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/isort[>=4.2.5&<5][python_abis:*(-)?]
        dev-python/mccabe[>=0.6&<0.7][python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
    run:
        dev-python/logilab-common[python_abis:*(-)?]
    test-expensive:
        dev-python/pytest-runner[python_abis:*(-)?]
"

if ever at_least 2; then
    DEPENDENCIES+="
        build+run:
            dev-python/astroid[>=2.2.0&<3][python_abis:*(-)?]
    "
else
    DEPENDENCIES+="
        build+run:
            dev-python/astroid[~>1.6][python_abis:*(-)?]
        python_abis:2.7? (
            dev-python/backports-functools_lru_cache[python_abis:2.7]
            dev-python/configparser[python_abis:2.7]
            dev-python/singledispatch[python_abis:2.7]
        )
    "
fi

# those contain errors for testing pylint. Obviously, those are breaking byte-compilation
PYTHON_BYTECOMPILE_EXCLUDES=( test/functional test/input test/regrtest_data )

# Tests take a looong time
# XXX 48 seconds in 2.2.0, but somewhat longer for 1.9.2
RESTRICT="test"

pylint_src_test_expensive() {
    easy-multibuild_run_phase
}

test_expensive_one_multibuild() {
    esandbox allow_net "unix:${TEMP%/}/pymp-*/listener-*"
    esandbox allow_net --connect "unix:${TEMP%/}/pymp-*/listener-*"

    setup-py_test_one_multibuild

    esandbox disallow_net "unix:${TEMP%/}/pymp-*/listener-*"
    esandbox disallow_net --connect "unix:${TEMP%/}/pymp-*/listener-*"
}

