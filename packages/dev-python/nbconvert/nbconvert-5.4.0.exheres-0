# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

# Python 3 only because ipython is the only dependent.
require pypi utf8-locale setup-py [ blacklist=2 import=setuptools test=pytest ]

SUMMARY="Converting Jupyter Notebooks"
HOMEPAGE+=" https://ipython.org"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/bleach[python_abis:*(-)?]
        dev-python/defusedxml[python_abis:*(-)?]
        dev-python/entrypoints[>=0.2.2][python_abis:*(-)?]
        dev-python/Jinja2[python_abis:*(-)?]
        dev-python/jupyter_core[python_abis:*(-)?]
        dev-python/mistune[>=0.8.1][python_abis:*(-)?]
        dev-python/nbformat[>=4.4][python_abis:*(-)?]
        dev-python/pandocfilters[>=1.4.1][python_abis:*(-)?]
        dev-python/Pygments[python_abis:*(-)?]
        dev-python/testpath[python_abis:*(-)?]
        dev-python/traitlets[>=4.2][python_abis:*(-)?]
    test:
        dev-python/ipykernel[python_abis:*(-)?]
        dev-python/nose[python_abis:*(-)?]
        dev-python/jupyter_client[>=4.2][python_abis:*(-)?]
        dev-python/pytest-cov[python_abis:*(-)?]
"

PYTEST_PARAMS=(
    -v
    -k "not test_run_notebooks"  # broken test, last checked: 5.4.0
)

# Fails tests if not installed yet
RESTRICT="test"

pkg_setup() {
    require_utf8_locale
}

